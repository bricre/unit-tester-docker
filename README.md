Inspired by [tiangolo/docker-with-compose](https://github.com/tiangolo/docker-with-compose)

This image can be used as part of Jenkins CI process running on Kubernetes cluster.

It provides:

docker
docker-compose
bash
GNU parallel